# utility

collection of hopefully useful and reusable functions

## Installation

```bash
pip install git+https://gitlab.physik.uni-muenchen.de/David.Koch/utility.git
```

or
```bash
pip install "utility[dask] @ git+https://gitlab.physik.uni-muenchen.de/David.Koch/utility.git"
```
if you want to use the `utility.slurm` module

or
```bash
git clone https://gitlab.physik.uni-muenchen.de/David.Koch/utility.git
cd utility
pip install -e .
```

## Usage

Read the [docs](docs.md)

## Build docs

```bash
pip install pydoc-markdown
pydoc-markdown > docs.md
```
