# Table of Contents

* [utility](#utility)
  * [human\_bytes](#utility.human_bytes)
  * [recursive\_bases](#utility.recursive_bases)
  * [json\_generator](#utility.json_generator)
  * [jsonify](#utility.jsonify)
  * [groupby\_dictkey](#utility.groupby_dictkey)
  * [Cursor](#utility.Cursor)
    * [move](#utility.Cursor.move)
    * [peek](#utility.Cursor.peek)
* [utility.stopwatch](#utility.stopwatch)
  * [Marker](#utility.stopwatch.Marker)
    * [serialize](#utility.stopwatch.Marker.serialize)
  * [Stopwatch](#utility.stopwatch.Stopwatch)
    * [start](#utility.stopwatch.Stopwatch.start)
    * [set\_marker](#utility.stopwatch.Stopwatch.set_marker)
    * [stop](#utility.stopwatch.Stopwatch.stop)
    * [get\_report](#utility.stopwatch.Stopwatch.get_report)
    * [print\_report](#utility.stopwatch.Stopwatch.print_report)
    * [save](#utility.stopwatch.Stopwatch.save)
  * [sum\_records](#utility.stopwatch.sum_records)
* [utility.log](#utility.log)
  * [logged\_function](#utility.log.logged_function)
  * [logged\_class](#utility.log.logged_class)
* [utility.vmtouch](#utility.vmtouch)
  * [vmtouch](#utility.vmtouch.vmtouch)
* [utility.slurm](#utility.slurm)
  * [simple\_submit](#utility.slurm.simple_submit)
  * [get\_dask\_client](#utility.slurm.get_dask_client)

<a id="utility"></a>

# utility

<a id="utility.human_bytes"></a>

#### human\_bytes

```python
def human_bytes(size_bytes: int) -> str
```

convert ``size_bytes`` into human readable format

<a id="utility.recursive_bases"></a>

#### recursive\_bases

```python
def recursive_bases(cls) -> dict
```

returns a tree of all parent classes of a class

<a id="utility.json_generator"></a>

#### json\_generator

```python
def json_generator(files: List[str]) -> Iterable[dict]
```

iterate over every file in files, read it as json and yield.
useful when reading a large number of json files in sequence

<a id="utility.jsonify"></a>

#### jsonify

```python
def jsonify(d: Any) -> Any
```

create a json-dumpable object from the input by calling `str()` on any non-json-type item

<a id="utility.groupby_dictkey"></a>

#### groupby\_dictkey

```python
def groupby_dictkey(d: Dict[Any, Dict[T, Any]],
                    key: T) -> Dict[T, Dict[Any, Dict[T, Any]]]
```

group equally structured dictionaries by a common key

example:

```python
>>> files = {
...     "sample1.root": {"process": "ZZ", "generator": "XYZ", ...},
...     "sample2.root": {"process": "WZ", "generator": "XXZ", ...},
...     "sample3.root": {"process": "ZZ", "generator": "ZZZ", ...},
...     ...
... }
>>> groupby_dictkey(files, "process")
{
    "ZZ": {
        "sample1.root": {"process": "ZZ", "generator": "XYZ", ...},
        "sample3.root": {"process": "ZZ", "generator": "ZZZ", ...},
    },
    "WZ": {
        "sample2.root": {"process": "WZ", "generator": "XXZ", ...},
    },
    ...
}
```

<a id="utility.Cursor"></a>

## Cursor Objects

```python
class Cursor(Generic[T])
```

move index-wise through iterable and remember position

*paramters*:
 - `data: Iterable`: data to iterate through
 - `start: int = 0`: initial cursor position

<a id="utility.Cursor.move"></a>

#### move

```python
def move(range: int) -> Iterable[T]
```

move the cursor `range` forwards and return the slice from the old to the new index

<a id="utility.Cursor.peek"></a>

#### peek

```python
def peek(range: int) -> Iterable[T]
```

look `range` forwards without moving the cursor

<a id="utility.stopwatch"></a>

# utility.stopwatch

simple helper to measure execution times in scripts

**Usage**:
```python
from utility.stopwatch import Stopwatch

timer = Stopwatch(initial_marker="setup")

timer.start()
setup_function()
timer.set_marker("heavy computation 1")
heavy_computation(data)
timer.set_marker("io stuff")
data.write_to_disc()

timer.stop()
timer.print_report()
timer.save("timings.json")
```

<a id="utility.stopwatch.Marker"></a>

## Marker Objects

```python
@dataclass
class Marker()
```

represent a section recorded by a stopwatch

<a id="utility.stopwatch.Marker.serialize"></a>

#### serialize

```python
def serialize() -> Dict[str, Any]
```

serialize self into json

<a id="utility.stopwatch.Stopwatch"></a>

## Stopwatch Objects

```python
class Stopwatch()
```

track timestamps created by the `set_marker` method.

*parameters*:
 - `initial_marker: str = "timer"`: the name of the initial marker

<a id="utility.stopwatch.Stopwatch.start"></a>

#### start

```python
def start()
```

start timer

<a id="utility.stopwatch.Stopwatch.set_marker"></a>

#### set\_marker

```python
def set_marker(name: str = None)
```

set a marker with the name `name`. this records a current timestamp and
time difference to the previous marker

<a id="utility.stopwatch.Stopwatch.stop"></a>

#### stop

```python
def stop()
```

set a final marker

<a id="utility.stopwatch.Stopwatch.get_report"></a>

#### get\_report

```python
def get_report() -> dict
```

get a summary of all markers in json format

<a id="utility.stopwatch.Stopwatch.print_report"></a>

#### print\_report

```python
def print_report()
```

print a summary of all markers

<a id="utility.stopwatch.Stopwatch.save"></a>

#### save

```python
def save(path: str)
```

save the summary to a json file

<a id="utility.stopwatch.sum_records"></a>

#### sum\_records

```python
def sum_records(records: Iterable[dict]) -> dict
```

sum the measurements of multiple stopwatch-records

<a id="utility.log"></a>

# utility.log

<a id="utility.log.logged_function"></a>

#### logged\_function

```python
def logged_function(f: Callable) -> Callable
```

decorator for functions that logs a debug message on each call

<a id="utility.log.logged_class"></a>

#### logged\_class

```python
def logged_class(cls: type = None, exclude: list[str] = None) -> type
```

decorator for classes so that every call to a method produces a debug message.
methods listed in `exclude` will not be logged.

Use like this:
```python
@logged_class
class Foo: ...

# or

@logged_class(exclude=["baz"])
class Bar: ...
```

<a id="utility.vmtouch"></a>

# utility.vmtouch

<a id="utility.vmtouch.vmtouch"></a>

#### vmtouch

```python
def vmtouch(files: str, options="", executable="vmtouch") -> dict
```

run vmtouch with the given options, parse the output and return as a dict.
currently, only "-t" (load) and "-e" (evict) is supported

<a id="utility.slurm"></a>

# utility.slurm

utility for using our slurm cluster

<a id="utility.slurm.simple_submit"></a>

#### simple\_submit

```python
def simple_submit(commands: Dict[str, str], options: dict = None)
```

create submit script for `commands` and submit it to sbatch. `commands` should be a dict
mapping names of jobs to shell commands. `options` can contain a dictionary of sbatch options,
e.g. `{"partition": "ls-schaile"}`

example usage:

```python
from utility import slurm
import os

files = os.listdir("datadir")
commands = {file.split(".")[0]: f"python process.py {file}" for file in files}

slurm.simple_submit(commands, slurm.SBATCH_DEFAULT_OPTIONS)
```

<a id="utility.slurm.get_dask_client"></a>

#### get\_dask\_client

```python
def get_dask_client(cluster_options: dict = None) -> Client
```

get a Dask client for SLURM cluster. If no `cluster_options` are passed,
these defaults are used:
```python
dict(
    cores=1,
    queue="ls-schaile",
    memory="3GB",
    n_workers=50,
)
```

