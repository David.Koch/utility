from collections import defaultdict
import math
from typing import Any, Generic, Iterable, List, TypeVar, Dict
import json


def human_bytes(size_bytes: int) -> str:
    """convert ``size_bytes`` into human readable format"""
    # taken from https://stackoverflow.com/questions/5194057/better-way-to-convert-file-sizes-in-python
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return f"{s} {size_name[i]}"


def recursive_bases(cls) -> dict:
    """
    returns a tree of all parent classes of a class
    """
    parents = {}
    for parent in cls.__bases__:
        parents[parent] = recursive_bases(parent)
    return parents


def json_generator(files: List[str]) -> Iterable[dict]:
    """iterate over every file in files, read it as json and yield.
    useful when reading a large number of json files in sequence
    """
    for file in files:
        with open(file) as f:
            yield json.load(f)


def jsonify(d: Any) -> Any:
    """create a json-dumpable object from the input by calling `str()` on any non-json-type item"""
    json_types = [str, int, float, bool, None]
    if any(isinstance(d, typ) for typ in json_types[:-1]) or d is None:
        return d
    elif isinstance(d, dict):
        return {jsonify(k): jsonify(v) for k, v in d.items()}
    elif isinstance(d, list):
        return [jsonify(v) for v in d]
    else:
        return str(d)


T = TypeVar("T")


def groupby_dictkey(
    d: Dict[Any, Dict[T, Any]], key: T
) -> Dict[T, Dict[Any, Dict[T, Any]]]:
    """group equally structured dictionaries by a common key

    example:

    ```python
    >>> files = {
    ...     "sample1.root": {"process": "ZZ", "generator": "XYZ", ...},
    ...     "sample2.root": {"process": "WZ", "generator": "XXZ", ...},
    ...     "sample3.root": {"process": "ZZ", "generator": "ZZZ", ...},
    ...     ...
    ... }
    >>> groupby_dictkey(files, "process")
    {
        "ZZ": {
            "sample1.root": {"process": "ZZ", "generator": "XYZ", ...},
            "sample3.root": {"process": "ZZ", "generator": "ZZZ", ...},
        },
        "WZ": {
            "sample2.root": {"process": "WZ", "generator": "XXZ", ...},
        },
        ...
    }
    ```
    """
    grouped = defaultdict(dict)
    for k, v in d.items():
        grouped[v[key]][k] = v
    return dict(sorted(grouped.items()))


class Cursor(Generic[T]):
    """move index-wise through iterable and remember position

    *paramters*:
     - `data: Iterable`: data to iterate through
     - `start: int = 0`: initial cursor position
    """

    def __init__(self, data: Iterable[T], start: int = 0):
        self.data = data
        self.position = start

    def move(self, range: int) -> Iterable[T]:
        """move the cursor `range` forwards and return the slice from the old to the new index"""
        d = self.peek(range)
        self.position += range
        return d

    def peek(self, range: int) -> Iterable[T]:
        """look `range` forwards without moving the cursor"""
        return self.data[self.position : self.position + range]
