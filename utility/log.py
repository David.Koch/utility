from __future__ import annotations
import logging
from functools import wraps
from typing import Callable

logger = logging.getLogger(__name__)


def logged_function(f: Callable) -> Callable:
    """decorator for functions that logs a debug message on each call"""

    @wraps(f)
    def logged_f(*args, **kwargs):
        result = f(*args, **kwargs)
        logger.debug(f"calling {f.__name__} with {args=}, {kwargs=}, returns {result}")
        return result

    return logged_f


def logged_class(cls: type = None, exclude: list[str] = None) -> type:
    """decorator for classes so that every call to a method produces a debug message.
    methods listed in `exclude` will not be logged.

    Use like this:
    ```python
    @logged_class
    class Foo: ...

    # or

    @logged_class(exclude=["baz"])
    class Bar: ...
    ```
    """
    exclude = exclude or []

    def _logged_class(cls):
        for attrname in dir(cls):
            # skip some specials __class__
            if attrname.startswith("__") or attrname in exclude:
                continue
            attr = getattr(cls, attrname)
            if callable(attr):
                setattr(cls, attrname, logged_function(attr))
        return cls

    if cls:
        return _logged_class(cls)
    else:
        return _logged_class
