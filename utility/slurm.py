"""utility for using our slurm cluster
"""


from __future__ import annotations
import os
from pathlib import Path
import subprocess
from datetime import datetime
from typing import Dict
import logging

logger = logging.getLogger(__name__)

try:
    from dask.distributed import Client
    from dask_jobqueue import SLURMCluster
except ImportError:
    logger.warning("""'dask' and 'dask_jobqueue' are not installed, cannot use 'utility.slurm'. use

        pip install dask dask_jobqueue

to install them.""")


SBATCH_DEFAULT_OPTIONS = {
    "partition": "ls-schaile",
    "output": "{outputpath}",
    "error": "{errorpath}",
}


def format_sbatch_options(options: dict, **kwargs) -> str:
    options = "\n".join([f"#SBATCH --{opt}={val}" for opt, val in options.items()])
    return options.format(**kwargs)


job_template = """#!/bin/bash

# this submit script was automatically generated on {time}

{sbatch_options}
{command}
"""


def create_submission_script(
    directory: Path, name: str, command: str, options: dict
) -> Path:
    sbatch_options = format_sbatch_options(
        options,
        outputpath=(directory / name).with_suffix(".out"),
        errorpath=(directory / name).with_suffix(".err"),
    )
    job = job_template.format(
        time=datetime.now(),
        sbatch_options=sbatch_options,
        command=command,
    )
    file_path = (directory / name).with_suffix(".sh")
    with open(file_path, "w") as of:
        of.write(job)
    return file_path


def simple_submit(commands: Dict[str, str], options: dict = None):
    """create submit script for `commands` and submit it to sbatch. `commands` should be a dict
    mapping names of jobs to shell commands. `options` can contain a dictionary of sbatch options,
    e.g. `{"partition": "ls-schaile"}`

    example usage:

    ```python
    from utility import slurm
    import os

    files = os.listdir("datadir")
    commands = {file.split(".")[0]: f"python process.py {file}" for file in files}

    slurm.simple_submit(commands, slurm.SBATCH_DEFAULT_OPTIONS)
    ```
    """
    options = options or {}
    job_dir = Path(os.getcwd()) / ".job"
    job_dir.mkdir(exist_ok=True)

    for name, command in commands.items():
        script_path = create_submission_script(job_dir, name, command, options)
        print(f"submit script for {command=}")
        subprocess.call(["sbatch", script_path])


def get_dask_client(cluster_options: dict = None) -> Client:
    """get a Dask client for SLURM cluster. If no `cluster_options` are passed,
    these defaults are used:
    ```python
    dict(
        cores=1,
        queue="ls-schaile",
        memory="3GB",
        n_workers=50,
    )
    ```
    """
    default_options = dict(
        cores=1,
        queue="ls-schaile",
        memory="3GB",
        n_workers=50,
    )
    custom_options = cluster_options or {}
    options = {**default_options, **custom_options}

    cluster = SLURMCluster(**options)
    client = Client(cluster)
    return client
