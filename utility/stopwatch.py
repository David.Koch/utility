"""
simple helper to measure execution times in scripts

**Usage**:
```python
from utility.stopwatch import Stopwatch

timer = Stopwatch(initial_marker="setup")

timer.start()
setup_function()
timer.set_marker("heavy computation 1")
heavy_computation(data)
timer.set_marker("io stuff")
data.write_to_disc()

timer.stop()
timer.print_report()
timer.save("timings.json")
```
"""

from dataclasses import dataclass
from datetime import datetime, timedelta
import json
from collections import Counter
from typing import Iterable, Dict, Any, List


@dataclass
class Marker:
    """represent a section recorded by a stopwatch"""

    name: str
    absolute_time: datetime  # absolute time
    time: timedelta  # timedifference to previous marker

    def serialize(self) -> Dict[str, Any]:
        """serialize self into json"""
        return {
            "name": self.name,
            "absolute_time": self.absolute_time.isoformat(),
            "time": self.time.total_seconds(),
        }


class Stopwatch:
    """track timestamps created by the `set_marker` method.

    *parameters*:
     - `initial_marker: str = "timer"`: the name of the initial marker
    """

    def __init__(self, initial_marker: str = "timer"):
        self.markers: List[Marker] = []
        self.marker_name = initial_marker
        self.timestamp = None

    def start(self):
        """start timer"""
        self.timestamp = datetime.now()

    def set_marker(self, name: str = None):
        """set a marker with the name `name`. this records a current timestamp and
        time difference to the previous marker
        """
        now = datetime.now()
        if self.timestamp is not None:
            self.markers.append(
                Marker(
                    name=self.marker_name,
                    absolute_time=now,
                    time=(now - self.timestamp),
                )
            )
        if name:
            self.marker_name = name
        self.timestamp = now

    def stop(self):
        """set a final marker"""
        self.set_marker()

    def get_report(self) -> dict:
        """get a summary of all markers in json format"""
        total = sum([t.time.total_seconds() for t in self.markers])
        return {
            "timestamps": [marker.serialize() for marker in self.markers],
            "total_time": total,
        }

    def print_report(self):
        """print a summary of all markers"""
        total = sum([m.time.total_seconds() for m in self.markers])
        indent = max([len(m.name) for m in self.markers]) + 3
        print("---------------- Stopwatch ----------------")
        for marker in self.markers:
            print(f"{marker.name:{indent}}{marker.time.total_seconds()}s")
        print()
        print(f"{'total':{indent}}{total}s")

    def save(self, path: str):
        """save the summary to a json file"""
        report = self.get_report()
        with open(path, "w") as f:
            json.dump(report, f, indent=2, sort_keys=True)


def sum_records(records: Iterable[dict]) -> dict:
    """sum the measurements of multiple stopwatch-records"""
    summed_record = {
        "total_times": Counter(),
        "total_time": 0,
    }
    n = 0
    for record in records:
        summed_record["total_times"] += Counter(dict(record["timestamps"]))
        summed_record["total_time"] += record["total_time"]
        n += 1
    # convert counter back to dict for json compability
    summed_record["total_times"] = dict(summed_record["total_times"])
    # compute average times as well and save number of records
    summed_record["avg_times"] = {
        k: v / n for k, v in summed_record["total_times"].items()
    }
    summed_record["avg_time"] = summed_record["total_time"] / n
    summed_record["n"] = n
    return summed_record
