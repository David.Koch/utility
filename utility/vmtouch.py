from subprocess import Popen, PIPE
import glob
import sys


def _parse_output(stdout: str) -> dict:
    parse_rules = {
        "Files": int,
        "Directories": int,
        # resident pages has at its last value a percentage, convert it to a float
        "Resident Pages": lambda x: (
            x.split()[0],
            x.split()[1],
            float(x.split()[2][:-1]) / 100,
        ),
        "Elapsed": lambda x: float(x.split()[0]),
        "Touched Pages": lambda x: (int(x.split()[0]), x.split()[1]),
        "Evicted Pages": lambda x: (int(x.split()[0]), x.split()[1]),
    }
    output = stdout.splitlines()
    parsed = {}
    for line in output:
        key, value = line.split(":")
        key = key.strip()
        parsed[key] = parse_rules[key](value.strip())

    return parsed


def vmtouch(files: str, options="", executable="vmtouch") -> dict:
    """run vmtouch with the given options, parse the output and return as a dict.
    currently, only "-t" (load) and "-e" (evict) is supported
    """
    files = glob.glob(files)
    command = [executable, *files, options]
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    stderr = stderr.decode(sys.stdout.encoding)
    stdout = stdout.decode(sys.stdout.encoding)
    try:
        parsed = _parse_output(stdout)
    except:
        raise RuntimeError(
            f"could not parse output of command '{' '.join(command)}':\n{stderr}{stdout}"
        )

    return parsed
